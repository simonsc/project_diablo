// ----------------------------------------------------------------------------
// Quest items

ItemDisplay[leg]: %MAP-C6%%ORANGE%%NAME%%WHITE%		// Wirt's Leg
ItemDisplay[hdm]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Horadric Malus
ItemDisplay[bks]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Scroll of Inifuss
ItemDisplay[bkd]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Scroll of Inifuss, deciphered

ItemDisplay[ass]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Book of Skill
ItemDisplay[box]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Horadric Cube
ItemDisplay[tr1]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Horadric Scroll
ItemDisplay[msf]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Staff of Kings
ItemDisplay[hst]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Horadric Staff
ItemDisplay[vip]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Amulet of the Viper

ItemDisplay[xyz]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Potion of Life
ItemDisplay[j34]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// A Jade Figurine
ItemDisplay[g34]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// The Golden Bird
ItemDisplay[bbb]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Lam Esen's Tome
ItemDisplay[g33]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Gidbinn
ItemDisplay[qf1]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Khalim's Flail
ItemDisplay[qf2]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Khalim's Will
ItemDisplay[qey]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Khalim's Eye
ItemDisplay[qhr]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Khalim's Heart
ItemDisplay[qbr]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Khalim's Brain
ItemDisplay[mss]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Mephisto's Soulstone

ItemDisplay[hfh]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Hellforge Hammer

ItemDisplay[ice]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Malah's Potion
ItemDisplay[tr2]: %MAP-B9%%PURPLE%%NAME%%WHITE%		// Scroll of Resistance

// ----------------------------------------------------------------------------
// Essences

ItemDisplay[bet]: %MAP-C6%%ORANGE%%NAME%%WHITE%		// Burning Essence of Terror
ItemDisplay[ceh]: %MAP-C6%%ORANGE%%NAME%%WHITE%		// Charged Essence of Hatred
ItemDisplay[fed]: %MAP-C6%%ORANGE%%NAME%%WHITE%		// Festering Essence of Destruction
ItemDisplay[tes]: %MAP-C6%%ORANGE%%NAME%%WHITE%		// Twisted Essence of Suffering

// ----------------------------------------------------------------------------
// Tokens

ItemDisplay[toa]: %ORANGE%Token of Absolution%MAP-C6%	// Token of Absolution

// ----------------------------------------------------------------------------
// Pandemonium Keys

ItemDisplay[pk3]: %MAP-C6%%ORANGE%%NAME% [%QTY%]%WHITE%	// Key of Destruction
ItemDisplay[pk2]: %MAP-C6%%ORANGE%%NAME% [%QTY%]%WHITE%	// Key of Hate
ItemDisplay[pk1]: %MAP-C6%%ORANGE%%NAME% [%QTY%]%WHITE%	// Key of Terror

// ----------------------------------------------------------------------------
// Pandemonium Organs

ItemDisplay[bey]: %MAP-C6%%ORANGE%%NAME% [%QTY%]%WHITE%	// Baal's Eye
ItemDisplay[dhn]: %MAP-C6%%ORANGE%%NAME% [%QTY%]%WHITE%	// Diablo's Horn
ItemDisplay[mbr]: %MAP-C6%%ORANGE%%NAME% [%QTY%]%WHITE%	// Mephisto's Brain

// ----------------------------------------------------------------------------
// Corruption Items

ItemDisplay[wss]: %MAP-62%%PURPLE%+%RED%%NAME%%PURPLE%+ [%QTY%]%WHITE%	// Worldstone Shard
ItemDisplay[lbox]: %MAP-62%%PURPLE%+%RED%%NAME%%PURPLE%+%WHITE%			// Larzuk's Box

// ----------------------------------------------------------------------------
// Map Items

ItemDisplay[rera]: %NAME%{%TAN%Cube w/ Perfect Gem, Thul Rune or Greater, & Rare Map}		// Reroll Rare Map (Horadrim Orb)
ItemDisplay[imra]: %NAME%{%TAN%Cube w/ Jewel, Thul Rune or Greater, & White Map}			// Imbue Rare Map (Zakarum Orb)
ItemDisplay[upma]: %NAME%{%TAN%Cube w/ Perfect Skull, Thul Rune or Greater, & Magic Map}	// Upgrade Magic Map to Rare Map (Angelic Orb)
ItemDisplay[imma]: %NAME%{%TAN%Cube w/ Jewel, Any Rune, & White Map}						// Imbue Magic Map (Arcane Orb)
ItemDisplay[scou]: %NAME%{%TAN%Cube w/ Hel Rune & Magic or Rare Map}						// Purge Map (Removes Mods and Reverts back to a White Map) (Orb of Destruction)

// ----------------------------------------------------------------------------
// Maps

ItemDisplay[t11]: %MAP-62%%RED%++%WHITE%%NAME%%RED%++%WHITE%{%TAN%Right-click in Act 5 town to open%NL%Requires level 75} // Sewers of Harrogath Map
ItemDisplay[t12]: %MAP-62%%RED%++%WHITE%%NAME%%RED%++%WHITE%{%TAN%Right-click in Act 5 town to open%NL%Requires level 75} // Mesa Map
ItemDisplay[t13]: %MAP-62%%RED%++%WHITE%%NAME%%RED%++%WHITE%{%TAN%Right-click in Act 5 town to open%NL%Requires level 75} // Palace Map
ItemDisplay[t14]: %MAP-62%%RED%++%WHITE%%NAME%%RED%++%WHITE%{%TAN%Right-click in Act 5 town to open%NL%Requires level 75} // Ice Caves Map
ItemDisplay[t15]: %MAP-62%%RED%++%WHITE%%NAME%%RED%++%WHITE%{%TAN%Right-click in Act 5 town to open%NL%Requires level 75} // Horazons Memory Map
ItemDisplay[t16]: %MAP-62%%RED%++%WHITE%%NAME%%RED%++%WHITE%{%TAN%Right-click in Act 5 town to open%NL%Requires level 75} // Ruins of Viz-jun
ItemDisplay[t17]: %MAP-62%%RED%++%WHITE%%NAME%%RED%++%WHITE%{%TAN%Right-click in Act 5 town to open%NL%Requires level 75} // River of Blood
ItemDisplay[t18]: %MAP-62%%RED%++%WHITE%%NAME%%RED%++%WHITE%{%TAN%Right-click in Act 5 town to open%NL%Requires level 75} // Lava Map

// ----------------------------------------------------------------------------
// Diablo Clone Items

ItemDisplay[dcma]: %MAP-62%%RED%--%NAME%--%WHITE%{%TAN%Soul to fight Uber Diablo!%NL%Black Soulstone & Prime Evil%NL%Cube w/ Pure Demonic Essence,}	// Vision of Terror
ItemDisplay[dcbl]: %MAP-62%%RED%--%NAME%--%WHITE%{%TAN%Evil Soul to fight Uber Diablo!%NL%Black Soulstone & Prime%NL%Cube w/ Vision of Terror,}		// Pure Demonic Essence
ItemDisplay[dcho]: %MAP-62%%RED%--%NAME%--%WHITE%{%TAN%Evil Soul to fight Uber Diablo!%NL%Pure Demonic Essence & Prime%NL%Cube w/ Vision of Terror,}	// Black Soulstone
ItemDisplay[dcso]: %MAP-62%%RED%--%NAME%--%WHITE%{%TAN%Soulstone to fight Uber Diablo!%NL%Pure Demonic Essence & Black%NL%Cube w/ Vision of Terror,}	// Prime Evil Soul

// ----------------------------------------------------------------------------
// Hide gems when above certain level

//ItemDisplay[GEM=1 CLVL>666]:		// Chipped gems
//ItemDisplay[GEM=2 CLVL>666]:		// Flawed gems
//ItemDisplay[GEM=3 CLVL>666]:		// Normal gems
//ItemDisplay[GEM=4 CLVL>666]:		// Flawless gems
//ItemDisplay[GEM=5 CLVL>666]:		// Perfect gems

// ----------------------------------------------------------------------------
// Amethyst

ItemDisplay[gcv OR gcvs]: %PURPLE%+%WHITE%Chipped Amethyst%PURPLE%+%WHITE%				// Chipped Amethyst
ItemDisplay[gfv OR gfvs]: %PURPLE%+%WHITE%Flawed Amethyst%PURPLE%+%WHITE%				// Flawed Amethyst
ItemDisplay[gsv]: %PURPLE%+%WHITE%Amethyst%PURPLE%+%WHITE%								// Amethyst
ItemDisplay[gsvs]: %PURPLE%+%WHITE%Amethyst%PURPLE%+%WHITE%%CONTINUE%					// Amethyst Stacked
ItemDisplay[gzv]: %PURPLE%+%WHITE%Flawless Amethyst%PURPLE%+%WHITE%						// Flawless Amethyst
ItemDisplay[gzvs]: %PURPLE%+%WHITE%Flawless Amethyst%PURPLE%+%WHITE%%CONTINUE%			// Flawless Amethyst Stacked
ItemDisplay[gpv]: %DOT-B9%%PURPLE%+%WHITE%Perfect Amethyst%PURPLE%+%WHITE%				// Perfect Amethyst
ItemDisplay[gpvs]: %DOT-B9%%PURPLE%+%WHITE%Perfect Amethyst%PURPLE%+%WHITE%%CONTINUE%	// Perfect Amethyst Stacked

// ----------------------------------------------------------------------------
// Diamond

ItemDisplay[gcw OR gcws]: %WHITE%+%WHITE%Chipped Diamond%WHITE%+%WHITE%				// Chipped Diamond
ItemDisplay[gfw OR gfws]: %WHITE%+%WHITE%Flawed Diamond%WHITE%+%WHITE%				// Flawed Diamond
ItemDisplay[gsw]: %WHITE%+%WHITE%Diamond%WHITE%+%WHITE%								// Diamond
ItemDisplay[gsws]: %WHITE%+%WHITE%Diamond%WHITE%+%WHITE%%CONTINUE%					// Diamond Stacked
ItemDisplay[glw]: %WHITE%+%WHITE%Flawless Diamond%WHITE%+%WHITE%					// Flawless Diamond
ItemDisplay[glws]: %WHITE%+%WHITE%Flawless Diamond%WHITE%+%WHITE%%CONTINUE%			// Flawless Diamond Stacked
ItemDisplay[gpw]: %DOT-3A%%WHITE%+%WHITE%Perfect Diamond%WHITE%+%WHITE%				// Perfect Diamond
ItemDisplay[gpws]: %DOT-3A%%WHITE%+%WHITE%Perfect Diamond%WHITE%+%WHITE%%CONTINUE%	// Perfect Diamond Stacked

// ----------------------------------------------------------------------------
// Emerald

ItemDisplay[gcg OR gcgs]: %GREEN%+%WHITE%Chipped Emerald%GREEN%+%WHITE%				// Chipped Emerald
ItemDisplay[gfg OR gfgs]: %GREEN%+%WHITE%Flawed Emerald%GREEN%+%WHITE%				// Flawed Emerald
ItemDisplay[gsg]: %GREEN%+%WHITE%Emerald%GREEN%+%WHITE%								// Emerald
ItemDisplay[gsgs]: %GREEN%+%WHITE%Emerald%GREEN%+%WHITE%%CONTINUE%					// Emerald Stacked
ItemDisplay[glg]: %GREEN%+%WHITE%Flawless Emerald%GREEN%+%WHITE%					// Flawless Emerald
ItemDisplay[glgs]: %GREEN%+%WHITE%Flawless Emerald%GREEN%+%WHITE%%CONTINUE%			// Flawless Emerald Stacked
ItemDisplay[gpg]: %DOT-D7%%GREEN%+%WHITE%Perfect Emerald%GREEN%+%WHITE%				// Perfect Emerald
ItemDisplay[gpgs]: %DOT-D7%%GREEN%+%WHITE%Perfect Emerald%GREEN%+%WHITE%%CONTINUE%	// Perfect Emerald Stacked

// ----------------------------------------------------------------------------
// Ruby

ItemDisplay[gcr OR gcrs]: %RED%+%WHITE%Chipped Ruby%RED%+%WHITE%				// Chipped Ruby
ItemDisplay[gfr OR gfrs]: %RED%+%WHITE%Flawed Ruby%RED%+%WHITE%					// Flawed Ruby
ItemDisplay[gsr]: %RED%+%WHITE%Ruby%RED%+%WHITE%								// Ruby
ItemDisplay[gsrs]: %RED%+%WHITE%Ruby%RED%+%WHITE%%CONTINUE%						// Ruby Stacked
ItemDisplay[glr]: %RED%+%WHITE%Flawless Ruby%RED%+%WHITE%						// Flawless Ruby
ItemDisplay[glrs]: %RED%+%WHITE%Flawless Ruby%RED%+%WHITE%%CONTINUE%			// Flawless Ruby Stacked
ItemDisplay[gpr]: %DOT-05%%RED%+%WHITE%Perfect Ruby%RED%+%WHITE%				// Perfect Ruby
ItemDisplay[gprs]: %DOT-05%%RED%+%WHITE%Perfect Ruby%RED%+%WHITE%%CONTINUE%		// Perfect Ruby Stacked

// ----------------------------------------------------------------------------
// Sapphire

ItemDisplay[gcb OR gcbs]: %BLUE%+%WHITE%Chipped Sapphire%BLUE%+%WHITE%				// Chipped Sapphire
ItemDisplay[gfb OR gfbs]: %BLUE%+%WHITE%Flawed Sapphire%BLUE%+%WHITE%				// Flawed Sapphire
ItemDisplay[gsb]: %BLUE%+%WHITE%Sapphire%BLUE%+%WHITE%								// Sapphire
ItemDisplay[gsbs]: %BLUE%+%WHITE%Sapphire%BLUE%+%WHITE%%CONTINUE%					// Sapphire Stacked
ItemDisplay[glb]: %BLUE%+%WHITE%Flawless Sapphire%BLUE%+%WHITE%						// Flawless Sapphire
ItemDisplay[glbs]: %BLUE%+%WHITE%Flawless Sapphire%BLUE%+%WHITE%%CONTINUE%			// Flawless Sapphire Stacked
ItemDisplay[gpb]: %DOT-20%%BLUE%+%WHITE%Perfect Sapphire%BLUE%+%WHITE%				// Perfect Sapphire
ItemDisplay[gpbs]: %DOT-20%%BLUE%+%WHITE%Perfect Sapphire%BLUE%+%WHITE%%CONTINUE%	// Perfect Sapphire Stacked

// ------------------------------------------------------------------------------
// Topaz

ItemDisplay[gcy OR gcys]: %YELLOW%+%WHITE%Chipped Topaz%YELLOW%+%WHITE%				// Chipped Topaz
ItemDisplay[gfy OR gfys]: %YELLOW%+%WHITE%Flawed Topaz%YELLOW%+%WHITE%				// Flawed Topaz
ItemDisplay[gsy]: %YELLOW%+%WHITE%Topaz%YELLOW%+%WHITE%								// Topaz
ItemDisplay[gsys]: %YELLOW%+%WHITE%Topaz%YELLOW%+%WHITE%%CONTINUE%					// Topaz Stacked
ItemDisplay[gly]: %YELLOW%+%WHITE%Flawless Topaz%YELLOW%+%WHITE%					// Flawless Topaz
ItemDisplay[glys]: %YELLOW%+%WHITE%Flawless Topaz%YELLOW%+%WHITE%%CONTINUE%			// Flawless Topaz Stacked
ItemDisplay[gpy]: %DOT-F6%%YELLOW%+%WHITE%Perfect Topaz%YELLOW%+%WHITE%				// Perfect Topaz
ItemDisplay[gpys]: %DOT-F6%%YELLOW%+%WHITE%Perfect Topaz%YELLOW%+%WHITE%%CONTINUE%	// Perfect Topaz Stacked

// ----------------------------------------------------------------------------
// Skull

ItemDisplay[skc OR skcs]: %GRAY%+%WHITE%Chipped Skull%GRAY%+%WHITE%				// Chipped Skull
ItemDisplay[skf OR skfs]: %GRAY%+%WHITE%Flawed Skull%GRAY%+%WHITE%				// Flawed Skull
ItemDisplay[sku]: %GRAY%+%WHITE%Skull%GRAY%+%WHITE%								// Skull
ItemDisplay[skus]: %GRAY%+%WHITE%Skull%GRAY%+%WHITE%%CONTINUE%					// Skull Stacked
ItemDisplay[skls]: %GRAY%+%WHITE%Flawless Skull%GRAY%+%WHITE%					// Flawless Skull
ItemDisplay[skl]: %GRAY%+%WHITE%Flawless Skull%GRAY%+%WHITE%%CONTINUE%			// Flawless Skull Stacked
ItemDisplay[skzs]: %DOT-CD%%GRAY%+%WHITE%Perfect Skull%GRAY%+%WHITE%			// Perfect Skull
ItemDisplay[skz]: %DOT-CD%%GRAY%+%WHITE%Perfect Skull%GRAY%+%WHITE%%CONTINUE%	// Perfect Skull Stacked

// ----------------------------------------------------------------------------
// Add quantity of gem to name

ItemDisplay[GEM>0 QTY>1]: %NAME% [%QTY%]

// Health Potions
// ----------------------------------------------------------------------------

ItemDisplay[hp1 DIFF<1]: %RED%HP 1%WHITE%	// Minor Healing Potion
ItemDisplay[hp1 DIFF>0]: 					// Hide after Normal difficulty
ItemDisplay[hp2 DIFF<1]: %RED%HP 2%WHITE%	// Light Healing Potion
ItemDisplay[hp2 DIFF>0]: 					// Hide after Normal difficulty
ItemDisplay[hp3 DIFF<2]: %RED%HP 3%WHITE%	// Healing Potion
ItemDisplay[hp3 DIFF>1]: 					// Hide after Nightmare difficulty
ItemDisplay[hp4 DIFF<2]: %RED%HP 4%WHITE%	// Greater Healing Potion
ItemDisplay[hp4 DIFF>1]: 					// Hide after Nightmare difficulty
ItemDisplay[hp5]: %RED%HP 5%WHITE%			// Super Healing Potion

// ----------------------------------------------------------------------------
// Mana Potions

ItemDisplay[mp1 DIFF<1]: %BLUE%MP 1%WHITE%	// Minor Mana Potion
ItemDisplay[mp1 DIFF>0]: 					// Hide after Normal difficulty
ItemDisplay[mp2 DIFF<1]: %BLUE%MP 2%WHITE%	// Light Mana Potion
ItemDisplay[mp2 DIFF>0]: 					// Hide after Normal difficulty
ItemDisplay[mp3 DIFF<2]: %BLUE%MP 3%WHITE%	// Mana Potion
ItemDisplay[mp3 DIFF>1]: 					// Hide after Nightmare difficulty
ItemDisplay[mp4 DIFF<2]: %BLUE%MP 4%WHITE%	// Greater Mana Potion
ItemDisplay[mp4 DIFF>1]: 					// Hide after Nightmare difficulty
ItemDisplay[mp5]: %BLUE%MP 5%WHITE%			// Super Mana Potion

// ----------------------------------------------------------------------------
// Rejuv Potions

ItemDisplay[rvs]: %PURPLE%.%WHITE%Rejuv 1%PURPLE%.%WHITE%		// Rejuv Potion
ItemDisplay[rvl]: %PURPLE%.%WHITE%Rejuv 2%PURPLE%.%WHITE%		// Full Rejuv Potion

// ----------------------------------------------------------------------------
// Other Drinkable Potions

ItemDisplay[vps]: %WHITE%.%WHITE%Stam%WHITE%.%WHITE%		// Stamina Potion
ItemDisplay[yps]: %GRAY%.%WHITE%Anti%GRAY%.%WHITE%			// Antidote Potion
ItemDisplay[wms]: %YELLOW%.%WHITE%Thaw%YELLOW%.%WHITE%		// Thawing Potion

// ----------------------------------------------------------------------------
// Throwing Potions

ItemDisplay[gpl]: %GREEN%.%WHITE%Strang%GREEN%.%WHITE% [%QTY%]		// Strangling Potion
ItemDisplay[gpm]: %GREEN%.%WHITE%Chok%GREEN%.%WHITE% [%QTY%]		// Choking Potion
ItemDisplay[gps]: %GREEN%.%WHITE%Ranc%GREEN%.%WHITE% [%QTY%]		// Rancid Potion
ItemDisplay[opl]: %ORANGE%.%WHITE%Fulm%ORANGE%.%WHITE% [%QTY%]		// Fulminating Potion
ItemDisplay[opm]: %ORANGE%.%WHITE%Explo%ORANGE%.%WHITE% [%QTY%]		// Exploding Potion
ItemDisplay[ops]: %ORANGE%.%WHITE%Oil%ORANGE%.%WHITE% [%QTY%]		// Oil Potion

// ----------------------------------------------------------------------------
// Scrolls and Tomes

ItemDisplay[isc DIFF<1]: %RED%.%WHITE%ID%RED%.%WHITE%			// Scroll of Identify
ItemDisplay[isc DIFF>0]: 										// Hide after Normal difficulty
ItemDisplay[tsc DIFF<1]: %BLUE%.%WHITE%TP%BLUE%.%WHITE%			// Scroll of Town Portal
ItemDisplay[tsc DIFF>0]: 										// Hide after Normal difficulty
ItemDisplay[ibk]: %RED%.%WHITE%ID-T%RED%.%WHITE% [%QTY%]		// Tome of Identify
ItemDisplay[tbk]: %BLUE%.%WHITE%TP-T%BLUE%.%WHITE% [%QTY%]		// Tome of Town Portal

// ----------------------------------------------------------------------------
// Other Items

ItemDisplay[ear]: %NAME%
ItemDisplay[INF (ARMOR OR WEAPON) CLVL>10]:
ItemDisplay[aqv CLVL>10]:						// Arrows
ItemDisplay[cqv CLVL>10]:						// bolts

// ----------------------------------------------------------------------------
// Gold

ItemDisplay[GOLD<1000]:

// ----------------------------------------------------------------------------
// Eth/Sockets/Item Level

ItemDisplay[ETH]: %NAME% %RED%[Eth]%GRAY%%CONTINUE%				// Add "[Eth]" to ethereal item's names (eg "Quilted Armor [Eth]")
ItemDisplay[!RW !ETH SOCK>0]: %NAME% [%SOCKETS%]%CONTINUE%		// Add number of sockets to the end of socketed items' names (eg "Quilted Armor [2]")
ItemDisplay[!RW ETH SOCK>0]: %NAME%[%SOCKETS%]%CONTINUE%		// No space between eth and socket display if item is eth (eg "Quilted Armor [Eth][2]")
//ItemDisplay[!ETH SOCK=0 (MAG OR RARE OR ARMOR OR WEAPON OR amu OR rin)]: %NAME% [i%ILVL%]%CONTINUE% // Add ilvl to the end of items' names (eg "Quilted Armor [i99]")
//ItemDisplay[!RW (ETH OR SOCK>0) (MAG OR RARE OR ARMOR OR WEAPON OR amu OR rin)]: %NAME%[i%ILVL%]%CONTINUE% // No space between ilvl and other displays if item is eth (eg "Quilted Armor [Eth][2][i99]")

// ----------------------------------------------------------------------------
// Unique Miscellaneous

ItemDisplay[UNI !ID]: %MAP-A5%%NAME%

// ----------------------------------------------------------------------------
// Amulets

ItemDisplay[UNI !ID amu]: %MAP-A5%%NAME%	// All unique amulets

// ----------------------------------------------------------------------------
// Charms

ItemDisplay[UNI !ID cm1]: %MAP-A5%%NAME%	// Annihilus Small Charm
ItemDisplay[UNI !ID cm2]: %MAP-A5%%NAME%	// Hellfire Torch Large Charm
ItemDisplay[UNI !ID cm3]: %MAP-A5%%NAME%	// Gheed's Fortune Grand Charm

// ----------------------------------------------------------------------------
// Circlets

//ItemDisplay[UNI !ID ci0]: %MAP-A5%%NAME%		// Circlet (No Unique for this base)
//ItemDisplay[UNI !ID ci1]: %MAP-A5%%NAME%		// Coronet (No Unique for this base)
ItemDisplay[UNI !ID ci2]: %MAP-A5%%NAME%		// Kira's Guardian Tiara
ItemDisplay[UNI !ID ci3]: %MAP-A5%%NAME%		// Griffon's Eye Diadem

// ----------------------------------------------------------------------------
// Jewels

ItemDisplay[UNI !ID jew]: %MAP-A5%%NAME%	// Rainbow Facet Jewel

// ----------------------------------------------------------------------------
// Rings

ItemDisplay[UNI !ID rin]: %MAP-A5%%NAME%	// All unique rings

// ----------------------------------------------------------------------------
// Necromancer

// Normal tier Heads
//ItemDisplay[UNI !ID ne1]: %MAP-A5%%NAME%		// Preserved Head (No Unique for this base)
//ItemDisplay[UNI !ID ne2]: %MAP-A5%%NAME%		// Zombie Head (No Unique for this base)
//ItemDisplay[UNI !ID ne3]: %MAP-A5%%NAME%		// Unraveller Head (No Unique for this base)
//ItemDisplay[UNI !ID ne4]: %MAP-A5%%NAME%		// Gargoyle HEad (No Unique for this base)
//ItemDisplay[UNI !ID ne5]: %MAP-A5%%NAME%		// Demon Head (No Unique for this base)

// Exceptional tier Heads
//ItemDisplay[UNI !ID ne6]: %MAP-A5%%NAME%		// Mummified Trophy (No Unique for this base)
//ItemDisplay[UNI !ID ne7]: %MAP-A5%%NAME%		// Fetish Trophy (No Unique for this base)
//ItemDisplay[UNI !ID ne8]: %MAP-A5%%NAME%		// Sexton Trophy (No Unique for this base)
//ItemDisplay[UNI !ID ne9]: %MAP-A5%%NAME%		// Cantor Trophy (No Unique for this base)
ItemDisplay[UNI !ID nea]: %MAP-A5%%NAME%		// Homunculus Hierophant Trophy

// Elite tier Heads
//ItemDisplay[UNI !ID neb]: %MAP-A5%%NAME%		// Minion Skull (No Unique for this base)
//ItemDisplay[UNI !ID nec]: %MAP-A5%%NAME%		// Hellspawn Skull (No Unique for this base)
//ItemDisplay[UNI !ID ned]: %MAP-A5%%NAME%		// Overseer Skull (No Unique for this base)
ItemDisplay[UNI !ID nee]: %MAP-A5%%NAME%		// Boneflame Succubus Skull
ItemDisplay[UNI !ID nef]: %MAP-A5%%NAME%		// Darkforce Spawn Bloodlord Skull

// ----------------------------------------------------------------------------
// Normal Items

ItemDisplay[WEAPON NMAG AND SOCK=0]:	// Any Weapon
//ItemDisplay[AXE NORM]: 		// Axes
//ItemDisplay[MACE NORM]: 		// Maces
//ItemDisplay[SWORD NORM]: 		// Swords
//ItemDisplay[DAGGER NORM]: 	// Daggers
//ItemDisplay[THROWING NORM]: 	// Throwing Weapons
//ItemDisplay[JAV NORM]: 		// javelins
//ItemDisplay[SPEAR NORM]: 		// Spears
//ItemDisplay[POLEARM NORM]: 	// Polearms
//ItemDisplay[BOW NORM]: 		// Bows
//ItemDisplay[XBOW NORM]: 		// Crossbows
//ItemDisplay[STAFF NORM]: 		// Staves
//ItemDisplay[WAND NORM]: 		// Wands
//ItemDisplay[SCEPTER NORM]: 	// Scepters

ItemDisplay[ARMOR NMAG AND SOCK=0]: 	// Any Armor
//ItemDisplay[HELM NORM]: 		// Helms
//ItemDisplay[CHEST NORM]: 		// Body Armor
//ItemDisplay[SHIELD NORM]: 	// Shields
//ItemDisplay[GLOVES NORM]: 	// Gloves
//ItemDisplay[BOOTS NORM]: 		// Boots
//ItemDisplay[BELT NORM]: 		// Belts
//ItemDisplay[CIRC NORM]: 		// Circlets

// ----------------------------------------------------------------------------
// Runes

ItemDisplay[r01]: %DOT-60%%NAME%			// El
ItemDisplay[r01s]: %DOT-60%%NAME% [%QTY%]	// El Stacked
ItemDisplay[r02]: %DOT-60%%NAME%			// Eld
ItemDisplay[r02s]: %DOT-60%%NAME% [%QTY%]	// Eld Stacked
ItemDisplay[r03]: %DOT-60%%NAME%			// Tir
ItemDisplay[r03s]: %DOT-60%%NAME% [%QTY%]	// Tir Stacked
ItemDisplay[r04]: %DOT-60%%NAME%			// Nef
ItemDisplay[r04s]: %DOT-60%%NAME% [%QTY%]	// Nef Stacked
ItemDisplay[r05]: %DOT-60%%NAME%			// Eth
ItemDisplay[r05s]: %DOT-60%%NAME% [%QTY%]	// Eth Stacked
ItemDisplay[r06]: %DOT-60%%NAME%			// Ith
ItemDisplay[r06s]: %DOT-60%%NAME% [%QTY%]	// Ith Stacked
ItemDisplay[r07]: %DOT-60%%NAME%			// Tal
ItemDisplay[r07s]: %DOT-60%%NAME% [%QTY%]	// Tal Stacked
ItemDisplay[r08]: %DOT-60%%NAME%			// Ral
ItemDisplay[r08s]: %DOT-60%%NAME% [%QTY%]	// Ral Stacked
ItemDisplay[r09]: %DOT-60%%NAME%			// Ort
ItemDisplay[r09s]: %DOT-60%%NAME% [%QTY%]	// Ort Stacked
ItemDisplay[r10]: %DOT-60%%NAME%			// Thul
ItemDisplay[r10s]: %DOT-60%%NAME% [%QTY%]	// Thul Stacked
ItemDisplay[r11]: %DOT-60%%NAME%			// Amn
ItemDisplay[r11s]: %DOT-60%%NAME% [%QTY%]	// Amn Stacked
ItemDisplay[r12]: %DOT-60%%NAME%			// Sol
ItemDisplay[r12s]: %DOT-60%%NAME% [%QTY%]	// Sol Stacked
ItemDisplay[r13]: %DOT-60%%NAME%			// Shael
ItemDisplay[r13s]: %DOT-60%%NAME% [%QTY%]	// Shael Stacked
ItemDisplay[r14]: %DOT-60%%NAME%			// Dol
ItemDisplay[r14s]: %DOT-60%%NAME% [%QTY%]	// Dol Stacked
ItemDisplay[r15]: %DOT-60%%NAME%			// Hel
ItemDisplay[r15s]: %DOT-60%%NAME% [%QTY%]	// Hel Stacked
ItemDisplay[r16]: %DOT-60%%NAME%			// Io
ItemDisplay[r16s]: %DOT-60%%NAME% [%QTY%]	// Io Stacked
ItemDisplay[r17]: %DOT-60%%NAME%			// Lum
ItemDisplay[r17s]: %DOT-60%%NAME% [%QTY%]	// Lum Stacked
ItemDisplay[r18]: %DOT-60%%NAME%			// Ko
ItemDisplay[r18s]: %DOT-60%%NAME% [%QTY%]	// Ko Stacked
ItemDisplay[r19]: %DOT-60%%NAME%			// Fal
ItemDisplay[r19s]: %DOT-60%%NAME% [%QTY%]	// Fal Stacked
ItemDisplay[r20]: %DOT-60%%NAME%			// Lem
ItemDisplay[r20s]: %DOT-60%%NAME% [%QTY%]	// Lem Stacked
ItemDisplay[r21]: %DOT-60%%NAME%			// Pul
ItemDisplay[r21s]: %DOT-60%%NAME% [%QTY%]	// Pul Stacked
ItemDisplay[r22]: %DOT-60%%NAME%			// Um
ItemDisplay[r22s]: %DOT-60%%NAME% [%QTY%]	// Um Stacked
ItemDisplay[r23]: %DOT-60%%NAME%			// Mal
ItemDisplay[r23s]: %DOT-60%%NAME% [%QTY%]	// Mal Stacked
ItemDisplay[r24]: %DOT-60%%NAME%			// Ist
ItemDisplay[r24s]: %DOT-60%%NAME% [%QTY%]	// Ist Stacked
ItemDisplay[r25]: %DOT-60%%NAME%			// Gul
ItemDisplay[r25s]: %DOT-60%%NAME% [%QTY%]	// Gul Stacked
ItemDisplay[r26]: %DOT-60%%NAME%			// Vex
ItemDisplay[r26s]: %DOT-60%%NAME% [%QTY%]	// Vex Stacked
ItemDisplay[r27]: %DOT-60%%NAME%			// Ohm
ItemDisplay[r27s]: %DOT-60%%NAME% [%QTY%]	// Ohm Stacked
ItemDisplay[r28]: %DOT-60%%NAME%			// Lo
ItemDisplay[r28s]: %DOT-60%%NAME% [%QTY%]	// Lo Stacked
ItemDisplay[r29]: %DOT-60%%NAME%			// Sur
ItemDisplay[r29s]: %DOT-60%%NAME% [%QTY%]	// Sur Stacked
ItemDisplay[r30]: %DOT-60%%NAME%			// Ber
ItemDisplay[r30s]: %DOT-60%%NAME% [%QTY%]	// Ber Stacked
ItemDisplay[r31]: %DOT-60%%NAME%			// Jah
ItemDisplay[r31s]: %DOT-60%%NAME% [%QTY%]	// Jah Stacked
ItemDisplay[r32]: %DOT-60%%NAME%			// Cham
ItemDisplay[r32s]: %DOT-60%%NAME% [%QTY%]	// Cham Stacked
ItemDisplay[r33]: %DOT-60%%NAME%			// Zod
ItemDisplay[r33s]: %DOT-60%%NAME% [%QTY%]	// Zod Stacked
